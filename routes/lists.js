const express = require('express')
const router = express.Router()
const passport = require('passport');

const List = require('../models/List')

const validateListInput = require('../validation/list');

// Get lists
router.get('/', passport.authenticate('jwt', {session: false}), (req, res) => {
    List.find({user: req.user.id})
        .then(list => res.json(list))
        .catch(err => console.log(err))
})

// Add List
router.post('/', passport.authenticate('jwt', {session: false}), (req, res) => {
    const { errors, isValid } = validateListInput(req.body);

    // Check Validation
    if (!isValid) {
        return res.status(400).json(errors);
    }
    
    const listFields = new List({
        user: req.user.id,
        text: req.body.text,
        type_list: req.body.type_list,
        type_time: req.body.type_time,
        time: req.body.time,
        check: req.body.check
    });

    listFields.save()
        .then(list => res.json(list))
        .catch(err => console.log(err))
})


//Delete List
router.delete('/:id', passport.authenticate('jwt', {session: false}), (req, res) => {
    List.findByIdAndRemove({_id: req.params.id})
        .then(res.json('Success'))
        .catch(err => console.log(err))
})

//Edit List
router.post('/:id', passport.authenticate('jwt', {session: false}), (req, res) => {
    const { errors, isValid } = validateListInput(req.body);

    // Check Validation
    if (!isValid) {
        return res.status(400).json(errors);
    }
    
    listFields = {}
    listFields.user = req.user.id
    listFields.text = req.body.text
    listFields.type_list = req.body.type_list
    listFields.type_time = req.body.type_time
    if(req.body.check) listFields.check = req.body.check
    listFields.time = req.body.time

    List.findById({_id: req.params.id})
        .then(list => {
            if(list){
                List.findOneAndUpdate(
                    { _id: req.params.id },
                    { $set: listFields },
                    { new: true }
                  ).then(list => res.json(list));
            } else {
                res.json({msg: 'Not have list'})
            }

        })
        .catch(err => res.status(404).json({msg: err}))
    
})


module.exports = router