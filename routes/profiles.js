const express = require('express');
const router = express.Router();
const gravatar = require('gravatar');
const passport = require('passport');

const Profile = require('../models/Profile')
const User = require('../models/User')
const List = require('../models/List')

// Add or Update Profile
router.post('/', passport.authenticate('jwt', {session: false}), (req, res) => {
    const profileFields = {};
        profileFields.user = req.user.id
        if (req.body.work) profileFields.work = req.body.work
        if (req.body.free_time) profileFields.free_time = req.body.free_time
        if (req.body.style) profileFields.style = req.body.style
        if (req.body.animal) profileFields.animal = req.body.animal

    Profile.findOne({ user: req.user.id })
        .then(profile => {
            if (profile) {
                // Update
                Profile.findOneAndUpdate(
                  { user: req.user.id },
                  { $set: profileFields },
                  { new: true }
                ).then(profile => res.json(profile));
            } else {
                // Create
                new Profile(profileFields).save()
                    .then(profile => res.json(profile))
            }
        })
})

// Delete Profile
router.delete(
    '/',
    passport.authenticate('jwt', { session: false }),
    (req, res) => {
      Profile.findOneAndRemove({ user: req.user.id }).then(() => {
        User.findOneAndRemove({ _id: req.user.id }).then(() =>
          res.json({ success: true })
        );
      });
    }
  );

// Get Profiles 'public
router.get('/', (req, res) => {
  Profile.find()
    .then(profiles => res.json(profiles))
    .catch(err => console.log(err))
})


module.exports = router;