const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateListInput(data) {
  let errors = {};

  data.text = !isEmpty(data.text) ? data.text : '';
  data.type_time = !isEmpty(data.type_time) ? data.type_time : '';
  data.type_list = !isEmpty(data.type_list) ? data.type_list : '';
  data.time = !isEmpty(data.time) ? data.time : '';


  if (Validator.isEmpty(data.text)) {
    errors.text = 'Text field is required';
  }

  if (Validator.isEmpty(data.type_time)) {
    errors.type_time = 'Type_time field is required';
  }

  if (Validator.isEmpty(data.type_list)) {
    errors.type_list = 'Type_list field is required';
  }

  if (Validator.isEmpty(data.time)) {
    errors.time = 'Time field is required';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
