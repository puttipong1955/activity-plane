const mongoose = require('mongoose')
const newSchema = mongoose.Schema

const listSchema = new newSchema({
    user: {
        type: newSchema.Types.ObjectId,
        ref: 'users'
      },
    text: {
        type: String,
        required: true
    },
    type_list: {
        type: String,
        required: true
    },
    type_time: {
        type: String,
        required: true
    },
    time: {
        type: Date,
        required: true
    },
    check: {
        type: Boolean,
        default: false
    },
    date: {
        type: Date,
        default: Date.now
    }
})

module.exports = List = mongoose.model('lists', listSchema);