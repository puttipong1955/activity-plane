const mongoose = require('mongoose')
const newSchema = mongoose.Schema

const profileSchema = new newSchema({
    user: {
        type: newSchema.Types.ObjectId,
        ref: 'users'
      },
    work: {
        type: String,
        required: true
    },
    free_time: {
        type: String,
        required: true
    },
    style: {
        type: String,
        required: true
    },
    animal: {
        type: Boolean,
        default: false
    },
    date: {
        type: Date,
        default: Date.now
    }
})

module.exports = Profile = mongoose.model('profiles', profileSchema);